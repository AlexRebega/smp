jmp code
include 'emu8086.inc'  
 
;coordinates (20,50 is start position)
x DW 20
y DW 50 
       

;for mouse
oldX dw -1
oldY dw 0


;MACRO used for drawing the map
deseneaza_perete_orizontal MACRO culoare,Xoffset,Yoffset,lungime,eticheta
    mov cx, Xoffset ; column
    mov dx, Yoffset ; row
    mov al, culoare ; color  

    eticheta:  mov ah, 0ch ; draw pixel
               int 10h
               dec cx
               cmp cx, lungime
    jae eticheta
ENDM 

deseneaza_perete_vertical MACRO culoare,Xoffset,Yoffset,inaltime,eticheta
    mov cx, Xoffset
    mov dx, Yoffset
    mov al, culoare
    eticheta: mov ah, 0ch
    int 10h
    dec dx
    cmp dx, inaltime
    ja eticheta
ENDM

deseneaza_masina PROC
    POP ax
    POP bx 
    mov cx, bx
    add cx, 2
    POP bx
    add bx, 2
    mov dx, bx
    push ax
    mov al, 12
    sari_v1: mov ah, 0ch
        sari_o1: mov ah, 0ch
        int 10h
        dec cx
        cmp cx, x
        jae sari_o1
    int 10h
    dec dx
    cmp dx,y 
    jae sari_v1 
    
    push [y]
    push [x]
    
    POP bx 
    mov cx, bx
    sub cx, 2
    POP bx
    sub bx, 2
    mov dx, bx
    mov al, 12
    sari_v2: mov ah, 0ch
        sari_o2: mov ah, 0ch
        int 10h
        inc cx
        cmp cx, x
        jae sari_o2
    int 10h
    inc dx
    cmp dx,y 
    jae sari_v2 
    
    push [y]
    push [x]
    
    POP bx 
    mov cx, bx
    add cx, 2
    POP bx
    sub bx, 2
    mov dx, bx
    mov al, 12
    sari_v3: mov ah, 0ch
        sari_o3: mov ah, 0ch
        int 10h
        dec cx
        cmp cx, x
        jae sari_o3
    int 10h
    inc dx
    cmp dx,y 
    jae sari_v3
      
    push [y]
    push [x]  
      
    POP bx 
    mov cx, bx
    sub cx, 2
    POP bx
    add bx, 2
    mov dx, bx
    mov al, 12
    sari_v4: mov ah, 0ch
        sari_o4: mov ah, 0ch
        int 10h
        inc cx
        cmp cx, x
        jbe sari_o4
    int 10h
    dec dx
    cmp dx,y 
    jae sari_v4   
RET    
deseneaza_masina ENDP  

sterge_masina PROC
    POP ax
    POP bx 
    mov cx, bx
    add cx, 2
    POP bx
    add bx, 2
    mov dx, bx
    push ax
    mov al, 0
    sari_vs1: mov ah, 0ch
        sari_os1: mov ah, 0ch
        int 10h
        dec cx
        cmp cx, x
        jae sari_os1
    int 10h
    dec dx
    cmp dx,y 
    jae sari_vs1 
    
    push [y]
    push [x]
    
    POP bx 
    mov cx, bx
    sub cx, 2
    POP bx
    sub bx, 2
    mov dx, bx
    mov al, 0
    sari_vs2: mov ah, 0ch
        sari_os2: mov ah, 0ch
        int 10h
        inc cx
        cmp cx, x
        jae sari_os2
    int 10h
    inc dx
    cmp dx,y 
    jae sari_vs2 
    
    push [y]
    push [x]
    
    POP bx 
    mov cx, bx
    add cx, 2
    POP bx
    sub bx, 2
    mov dx, bx
    mov al, 0
    sari_vs3: mov ah, 0ch
        sari_os3: mov ah, 0ch
        int 10h
        dec cx
        cmp cx, x
        jae sari_os3
    int 10h
    inc dx
    cmp dx,y 
    jae sari_vs3
      
    push [y]
    push [x]  
      
    POP bx 
    mov cx, bx
    sub cx, 2
    POP bx
    add bx, 2
    mov dx, bx
    mov al, 0
    sari_vs4: mov ah, 0ch
        sari_os4: mov ah, 0ch
        int 10h
        inc cx
        cmp cx, x
        jbe sari_os4
    int 10h
    dec dx
    cmp dx,y 
    jae sari_vs4   
RET    
sterge_masina ENDP 

; print a mesage
printme:

mov     cs:temp1, si  ; protect si register.

pop     si            ; get return address (ip).

push    ax            ; store ax register.

next_char:      
        mov     al, cs:[si]
        inc     si            ; next byte.
        cmp     al, 0
        jz      printed        
        mov     ah, 0eh       ; teletype function.
        int     10h
        jmp     next_char     ; loop.
printed:

pop     ax            ; re-store ax register.

; si should point to next command after
; the call instruction and string definition:
push    si            ; save new return address into the stack.

mov     si, cs:temp1  ; re-store si register.

ret
; variable to store original
; value of si register.
temp1  dw  ?

code: 

mov ah, 0
    mov al, 13h ;grafic mod 320x200
    int 10h  
    ;jmp aici:

    push y
    push x
    call deseneaza_masina  
    
    ;draw map
    deseneaza_perete_orizontal 15,90,40,1,e1 
    deseneaza_perete_orizontal 15,60,60,1,e2
    deseneaza_perete_vertical 15,90,70,10,e3
    deseneaza_perete_vertical 15,60,100,60,e4
    deseneaza_perete_orizontal 15,135,100,60,e5
    deseneaza_perete_vertical 15,120,150,30,e6
    deseneaza_perete_orizontal 15,130,30,120,e7 
    deseneaza_perete_orizontal 15,160,10,90,e8 
    deseneaza_perete_vertical 15,160,130,10,e9 
    deseneaza_perete_orizontal 15,280,70,145,e10
    deseneaza_perete_orizontal 15,180,150,120,e11
    deseneaza_perete_vertical 15,180,200,100,e12
    deseneaza_perete_orizontal 15,240,130,180,e13
    deseneaza_perete_vertical 15,280,170,70,e14 
    deseneaza_perete_orizontal 15,240,100,220,e15
    deseneaza_perete_orizontal 15,280,170,250,e16
    deseneaza_perete_vertical 15,250,170,150,e17
    deseneaza_perete_orizontal 15,250,150,200,e18
    deseneaza_perete_vertical 15,200,200,150,e19 
    
    
    ;draw finish point
    mov     dl, 23  ; current column.
    mov     dh, 24   ; current row. 
    
    mov bl, 10 ;6 ;current attributes
    ;set cursor position at (dl,dh)
    mov     ah, 02h
    int     10h                    
    ;draw 
    mov     al, '*'
    mov     bh, 0
    mov     cx, 1
    mov     ah, 09h
    int     10h
    
    mov     dl, 24  ; current column.
    mov     dh, 24   ; current row. 
    
    mov bl, 10 ;6 ;current attributes
    ;set cursor position at (dl,dh)
    mov     ah, 02h
    int     10h                    
    ;draw 
    mov     al, '*'
    mov     bh, 0
    mov     cx, 1
    mov     ah, 09h
    int     10h
    
    
    ;car_only:
    
    ;SOUND -> the race begins               
    mov dl, 07h
    mov ah, 2
    int 21h
    mov dl, 07h
    mov ah, 2
    int 21h
    mov dl, 07h
    mov ah, 2
    int 21h
    
    ;start race
    wait_for_key:
    
    push y
    push x
    call sterge_masina 
        
    ; get keystroke from keyboard:
    ; (remove from the buffer)
    mov     ah, 0
    int     16h

    ; print the key (for debug pourpouse)
    ;mov     ah, 0eh
    ;int     10h

    ; press 'esc' to exit:
    cmp     al, 1bh
    jz      exit 
    
    
    
    cmp al, 'd'
        je right 
    cmp al, 's' 
        je down
    cmp al, 'a'
        je left
    cmp al, 'w'
        je up
    ;jmp wait_for_key
    
    
    ;computing next position
    ; TODO: make it move on diagonal direction
    right:
        ;print the key (for debug pourpouse)
        ;mov     ah, 0eh
        ;int     10h 
        add [x], 10
        jmp racing
    down:
        add [y], 10
        jmp racing
    left:
        sub [x], 10
        jmp racing
    up:
        sub [y], 10
        jmp racing
    
    ;move car 
    racing:
     
        push y
        push x
        call deseneaza_masina
    
    ;verifica coliziune
    ;INT 10h
    ;subint : 0Dh - Read Grafic Pixel 
    mov cx, x
    mov dx, y
    ;draw a blue pixel (for debug and to mark trajectory) 
    ;mov al, 9
    ;mov ah, 0ch
    ;mov bh, 0
    ;int 10h  
    
    mov ah, 0dh
    INT 10h
    cmp al, 0
        jne coliziune
     ;;;  nu MERGE !!!

    jmp     wait_for_key
        


    exit:
    ; reset mouse and get its status:
    mov ax, 0
    int 33h
    cmp ax, 0
    ; display mouse cursor:
    ;mov ax, 1
    ;int 33h



check_mouse_button:
	mov ax, 3
	int 33h
	shr cx, 1       ; x/2 - in this mode the value of CX is doubled.
	cmp bx, 1
	jne xor_cursor:
	mov al, 13   ; pixel color
	jmp draw_pixel
	xor_cursor:
	cmp oldX, -1
	je not_required
	push cx
	push dx
	mov cx, oldX
	mov dx, oldY
	mov ah, 0dh     ; get pixel.
	int 10h
	xor al, 8   ; pixel color
	mov ah, 0ch     ; set pixel
	int 10h
	pop dx
	pop cx
	not_required:
	mov ah, 0dh     ; get pixel.
	int 10h
	xor al, 8   ; pixel color
	mov oldX, cx
	mov oldY, dx
	draw_pixel:
	mov ah, 0ch     ; set pixel
	int 10h
	check_esc_key:
	mov dl, 255
	mov ah, 6
	int 21h
	cmp al, 27      ; esc?
	jne check_mouse_button 
	
;game over !
coliziune:
    ;
    call   printme
       db 0xd,0xa,"You crashed ! Game Over ! ", 0 
    ;looser sound:
    mov dl, 07h
    mov ah, 2
    int 21h    
    mov dl, 07h
    mov ah, 2
    int 21h
; wait for keystroke
mov ah,00
int 16h