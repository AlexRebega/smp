# README #



### What is this repository for? ###

* School project
* This is a small game writen in Assembly for educational pourpose

### How do I get set up? ###

* Developed using 16 bit CPU emulator: EMU8086
* Project includes:
     -menu
     -drawing map
     - driving car using keys: W,A,S,D
     - sound at START / FINISH
     - Mesages
     - Colision detection

* It uses BIOS interrupts to generate sound and display various stuff 